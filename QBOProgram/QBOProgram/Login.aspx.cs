﻿using System;
using QBOProgram.Models;
using Utilities.Security;
using System.Web;

namespace QBOProgram
{
    public partial class Login : System.Web.UI.Page
    {
        protected void Page_Load(object sender, EventArgs e)
        {
            User u = null;
            string username = "";
            string password = "";

            if (Request.Cookies["UserSettings"] != null)
            {
                divInvalidLogin.Style.Remove("display");
                divInvalidLogin.Style.Add("display", "none");

                username = Request.Cookies["UserSettings"]["Username"];
                password = Request.Cookies["UserSettings"]["Password"];

                u = UserRepository.Get(AESEncryption.Decrypt(username), AESEncryption.Decrypt(password));

                if (u != null)
                {
                    Session["User"] = u;
                    Response.Redirect("Estimate.aspx");
                }
            }
        }

        protected void btnLogin_Click(object sender, EventArgs e)
        {
            if (txtUsername.Text == "" || txtPassword.Text == "")
            {
                divInvalidLogin.Style.Remove("display");
                divInvalidLogin.Style.Add("display", "inherit");
                return;
            }

            User u = UserRepository.Get(txtUsername.Text, txtPassword.Text);

            if (u != null)
            {
                divInvalidLogin.Style.Remove("display");
                divInvalidLogin.Style.Add("display", "none");

                if (cbRemember.Checked)
                {
                    if (Request.Cookies["UserSettings"] == null)
                        CreateCookie(txtUsername.Text, txtPassword.Text);
                }

                Session["User"] = u;
                // if (u.Level == 10)
                // Response.Redirect("Dashboard.aspx");
                //else
                Response.Redirect("Estimate.aspx");
            }
            else
            {
                divInvalidLogin.Style.Remove("display");
                divInvalidLogin.Style.Add("display", "inherit");
                //CreateCookie(txtUsername.Text, txtPassword.Text);
            }

        }

        private void CreateCookie(string username, string password)
        {
            HttpCookie cookie = new HttpCookie("UserSettings");
            cookie["Username"] = AESEncryption.Encrypt(username);
            cookie["Password"] = AESEncryption.Encrypt(password);
            cookie.Expires = DateTime.Now.AddYears(1);
            Response.Cookies.Add(cookie);
        }
    }
}