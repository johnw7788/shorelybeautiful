﻿using System;
using System.Collections.Generic;
using System.Configuration;
using System.Data;
using System.Data.SqlClient;
using System.Linq;
using System.Web;
using Utilities.Data;
using Utilities.Extensions;
using QBOProgram.Models;


    public static class UserRepository
    {
        public static string ConnectionString = CommonAccess.GetConnectionString("QBODataConnectionString");

        public static List<User> Get()
        {
            if (string.IsNullOrEmpty(ConnectionString))
                throw new ArgumentNullException("QBODataConnectionString", "Make sure 'QBODataConnectionString' connectionstring is set in the application's configuration file");

            if (string.IsNullOrEmpty(ConfigurationManager.AppSettings["Read_Users"]))
                throw new ArgumentNullException("Read_Users", "Make sure 'Read_Users' appSetting is provided and set in the application's configuration file");

            List<User> users = null;
            User u = null;

            DataTable dt = CommonAccess.GetData(ConnectionString, ConfigurationManager.AppSettings["Read_Users"].ToString(), null);

            if (dt != null && dt.Rows.Count > 0)
            {
                users = new List<User>();
                foreach (DataRow row in dt.Rows)
                {
                    u = ParseUser(row);

                    if (u != null)
                        users.Add(u);
                }
            }

            return users;
        }

        public static User Get(string username)
        {
            if (string.IsNullOrEmpty(ConnectionString))
                throw new ArgumentNullException("QBODataConnectionString", "Make sure 'QBODataConnectionString' connectionstring is set in the application's configuration file");

            if (string.IsNullOrEmpty(ConfigurationManager.AppSettings["Read_UserByUsername"]))
                throw new ArgumentNullException("Read_UserByUsername", "Make sure 'Read_UserByUsername' appSetting is provided and set in the application's configuration file");

            User u = null;
            SqlParameter[] sqlParams = new SqlParameter[1];
            sqlParams[0] = new SqlParameter("@username", username);

            DataTable dt = CommonAccess.GetData(ConnectionString, ConfigurationManager.AppSettings["Read_UserByUsername"].ToString(), sqlParams);

            if (dt != null && dt.Rows.Count > 0)
            {
                u = ParseUser(dt.Rows[0]);
            }

            return u;
        }

        public static User Get(string username, string password)
        {
            if (string.IsNullOrEmpty(ConnectionString))
                throw new ArgumentNullException("QBODataConnectionString", "Make sure 'QBODataConnectionString' connectionstring is set in the application's configuration file");

            if (string.IsNullOrEmpty(ConfigurationManager.AppSettings["Read_UserByUsernameAndPassword"]))
                throw new ArgumentNullException("Read_UserByUsernameAndPassword", "Make sure 'Read_UserByUsernameAndPassword' appSetting is provided and set in the application's configuration file");

            User u = null;
            SqlParameter[] sqlParams = new SqlParameter[2];
            sqlParams[0] = new SqlParameter("@username", username);
            sqlParams[1] = new SqlParameter("@password", password);

            DataTable dt = CommonAccess.GetData(ConnectionString, ConfigurationManager.AppSettings["Read_UserByUsernameAndPassword"].ToString(), sqlParams);

            if (dt != null && dt.Rows.Count > 0)
            {
                u = ParseUser(dt.Rows[0]);
            }

            return u;
        }

        public static List<User> Search(string searchField, string value)
        {
            if (string.IsNullOrEmpty(ConnectionString))
                throw new ArgumentNullException("CWTDispatchConnectionString", "Make sure 'CWTDispatchConnectionString' connectionstring is set in the application's configuration file");

            if (string.IsNullOrEmpty(ConfigurationManager.AppSettings["Read_SearchUsers"]))
                throw new ArgumentNullException("Read_SearchUsers", "Make sure 'Read_SearchUsers' appSetting is provided and set in the application's configuration file");

            List<User> users = null;
            User u = null;

            SqlParameter[] sqlParams = new SqlParameter[2];
            sqlParams[0] = new SqlParameter("@searchField", searchField);
            sqlParams[1] = new SqlParameter("@value", value);

            DataTable dt = CommonAccess.GetData(ConnectionString, ConfigurationManager.AppSettings["Read_SearchUsers"].ToString(), sqlParams);

            if (dt != null && dt.Rows.Count > 0)
            {
                users = new List<User>();
                foreach (DataRow row in dt.Rows)
                {
                    u = ParseUser(row);

                    if (u != null)
                        users.Add(u);
                }
            }

            return users;
        }

        public static void Create(User u)
        {
            if (string.IsNullOrEmpty(ConnectionString))
                throw new ArgumentNullException("CWTDispatchConnectionString", "Make sure 'CWTDispatchConnectionString' connectionstring is set in the application's configuration file");

            if (string.IsNullOrEmpty(ConfigurationManager.AppSettings["Create_User"]))
                throw new ArgumentNullException("Create_User", "Make sure 'Create_User' appSetting is provided and set in the application's configuration file");

            SqlParameter[] sqlParams = new SqlParameter[6];
            sqlParams[0] = new SqlParameter("@username", u.Username);
            sqlParams[1] = new SqlParameter("@password", u.Password);
            sqlParams[2] = new SqlParameter("@level", u.Level);
            sqlParams[3] = new SqlParameter("@email", u.Email);

            CommonAccess.ExecuteNonQuery(ConnectionString, ConfigurationManager.AppSettings["Create_User"].ToString(), sqlParams);

        }

        public static void Update(string previousUsername, User u)
        {
            if (string.IsNullOrEmpty(ConnectionString))
                throw new ArgumentNullException("CWTDispatchConnectionString", "Make sure 'CWTDispatchConnectionString' connectionstring is set in the application's configuration file");

            if (string.IsNullOrEmpty(ConfigurationManager.AppSettings["Update_User"]))
                throw new ArgumentNullException("Update_User", "Make sure 'Update_User' appSetting is provided and set in the application's configuration file");

            SqlParameter[] sqlParams = new SqlParameter[7];
            sqlParams[0] = new SqlParameter("@originalUsername", previousUsername);
            sqlParams[1] = new SqlParameter("@username", u.Username);
            sqlParams[2] = new SqlParameter("@password", u.Password);
            sqlParams[3] = new SqlParameter("@level", u.Level);
            sqlParams[4] = new SqlParameter("@email", u.Email);

            CommonAccess.ExecuteNonQuery(ConnectionString, ConfigurationManager.AppSettings["Update_User"].ToString(), sqlParams);
        }

        public static void Delete(string username)
        {
            if (string.IsNullOrEmpty(ConnectionString))
                throw new ArgumentNullException("CWTDispatchConnectionString", "Make sure 'CWTDispatchConnectionString' connectionstring is set in the application's configuration file");

            if (string.IsNullOrEmpty(ConfigurationManager.AppSettings["Delete_User"]))
                throw new ArgumentNullException("Delete_User", "Make sure 'Delete_User' appSetting is provided and set in the application's configuration file");

            SqlParameter[] sqlParams = new SqlParameter[1];
            sqlParams[0] = new SqlParameter("@Username", username);

            CommonAccess.ExecuteNonQuery(ConnectionString, ConfigurationManager.AppSettings["Delete_User"].ToString(), sqlParams);
        }

        private static User ParseUser(DataRow row)
        {
            try
            {
                User u = new User();
                u.Username = row.GetString("LoginName");
                u.Password = row.GetString("LoginPassword");
                u.Name = row.GetString("AGNTname");
                u.Level = row.GetInt32("UsrLevel");

                return u;

            }
            catch { return null; }
        }
    }