﻿<%@ Page Language="C#" AutoEventWireup="true" CodeBehind="NewEstimate.aspx.cs" Inherits="QBOProgram.NewEstimate" %>

<!DOCTYPE html>

<html xmlns="http://www.w3.org/1999/xhtml">
<head runat="server">
    <title></title>
</head>
<body>
    <form id="form1" runat="server">
        <asp:ScriptManager ID="ScriptManager1" runat="server"></asp:ScriptManager>
        <div>
            <asp:Panel ID="Panel1" runat="server">                
            <asp:Label ID="Label1" runat="server" Text="Label">New Estimate</asp:Label><br/>
            <asp:Button ID="btnEstimatePage" runat="server" Text="Back To Estimates Page" OnClick="btnEstimatePage_Click" /><br/>
            <asp:Button ID="btnEditEstimate" runat="server" Text="Edit/View Estimates" /><br/>
            </asp:Panel>
            <div>
        <asp:UpdatePanel ID="upLoad" runat="server" UpdateMode="Conditional">
        <ContentTemplate>
            <asp:GridView ID="gvNewEstimates" runat="server" AutoGenerateColumns="False" 
                Width="100%" 
                GridLines="None" 
                CssClass="Grid" 
                DataKeyNames="Id"
                OnRowDataBound="gvNewEstimates_RowDataBound"
                onselectedindexchanged="gvNewEstimates_SelectedIndexChanged"
                onpageindexchanging="gvNewEstimates_PageIndexChanging">
                <AlternatingRowStyle BackColor="#EEEEEE" />
                <HeaderStyle BackColor="LightSteelBlue" />
                <Columns>
                    <asp:BoundField DataField="Estimate" HeaderText="" SortExpression="Id" Visible="false" />
                    <asp:BoundField DataField="LineItem" HeaderText="Item" SortExpression="Id" />
                    <asp:BoundField DataField="ServiceDate" HeaderText="Service Date" />
                    <asp:BoundField DataField="ProductService" HeaderText="Product/Service" />
                    <asp:BoundField DataField="Description" HeaderText="Description" />
                    <asp:BoundField DataField="Quantity" HeaderText="QTY" />
                    <asp:BoundField DataField="Rate" HeaderText="Rate" DataFormatString="{0:C}" /> 
                    <asp:BoundField DataField="Amount" HeaderText="Amount" DataFormatString="{0:C}" />
                </Columns>
            </asp:GridView>
        </ContentTemplate>
        <Triggers>
            <asp:AsyncPostBackTrigger ControlID="btnSearch" EventName="Click" />
        </Triggers>
    </asp:UpdatePanel>
            </div>
        </div>
    </form>
</body>
</html>
