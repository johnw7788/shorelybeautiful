﻿<%@ Page Language="C#" AutoEventWireup="true" CodeBehind="Login.aspx.cs" Inherits="QBOProgram.Login" %>

<!DOCTYPE html>

<html xmlns="http://www.w3.org/1999/xhtml">
<head runat="server">
    <title>CWT Login</title>
    <link rel="stylesheet" media="screen" href="Styles/Bootstrap/css/bootstrap.css"  />
    <link rel="stylesheet" media="screen" href="Styles/signin.css"  />
</head>
<body>
    <div class="container">
        <form id="form1" runat="server" class="form-signin" >
            <h3 class="form-signin-heading">Shorely Beautiful Sign In</h3>
            <div class="control-group">
                <label class="control-label" for="inputEmail">Username</label>
                <div class="controls">
                    <asp:TextBox ID="txtUsername" runat="server" placeholder="Username" />
                </div>
            </div>
            <div class="control-group">
                <label class="control-label" for="inputEmail">Password</label>
                <div class="controls">
                    <asp:TextBox ID="txtPassword" runat="server" placeholder="Password" TextMode="Password" />
                </div>
            </div>
            <div class="control-group">
                <div class="controls">
                    <label class="checkbox">
                        <asp:CheckBox ID="cbRemember" runat="server" Text="Remember me" />
                    </label><br />
                    <asp:Button ID="btnLogin" runat="server" Text="Sign in" CssClass="btn btn-primary"
                        onclick="btnLogin_Click" />
                </div>
            </div>
            <div id="divInvalidLogin" runat="server" style="color:Red;display:none;">
                <asp:Label ID="lblInvalidLogin" runat="server" Text="Invalid login credentials" />
            </div>
        </form>
    </div>
</body>
</html>
