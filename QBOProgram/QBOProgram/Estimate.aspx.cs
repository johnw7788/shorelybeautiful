﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;
using QBOProgram.Models;

namespace QBOProgram
{
    public partial class Estimate : System.Web.UI.Page
    {
        protected void Page_Load(object sender, EventArgs e)
        {

        }

        protected void btnLogout_Click(object sender, EventArgs e)
        {
            if (Request.Cookies["UserSettings"] != null)
            {
                HttpCookie myCookie = new HttpCookie("UserSettings");
                myCookie.Expires = DateTime.Now.AddDays(-1d);
                Response.Cookies.Add(myCookie);
            }

            Response.Redirect("Login.aspx");
        }

        protected void btnNewEstimate_Click(object sender, EventArgs e)
        {
            Response.Redirect("NewEstimate.aspx");
        }

        protected void btnEditEstimate_Click(object sender, EventArgs e)
        {
            Response.Redirect("EditEstimate.aspx");
        }
    }
}