﻿<%@ Page Language="C#" AutoEventWireup="true" CodeBehind="Estimate.aspx.cs" Inherits="QBOProgram.Estimate" %>

<!DOCTYPE html>

<html xmlns="http://www.w3.org/1999/xhtml">
<head runat="server">
    <title></title>
</head>
<body>
    <form id="form1" runat="server">
        <div style="padding-top:5px;float:right;">
                <li class="dropdown" style="list-style-type:none;" >
                    <a class="dropdown-toggle" data-toggle="dropdown" href="#">
                    <asp:Label ID="lblUserName" runat="server" /><b class="caret"></b></a>
                    <ul class="dropdown-menu" style="left:-107px;">
                        <li><asp:LinkButton ID="btnLogout" runat="server" Text="Logout" 
                                onclick="btnLogout_Click" /></li>
                    </ul>
                </li>
            </div>
        <div>
            <asp:Panel ID="Panel1" runat="server">
                <asp:Label ID="Label1" runat="server" Text="Label">Estimates</asp:Label><br/>
                <asp:Button ID="btnNewEstimate" runat="server" Text="Create New" OnClick="btnNewEstimate_Click" /><br/>
                <asp:Button ID="btnEditEstimate" runat="server" Text="Edit/View" OnClick="btnEditEstimate_Click" /><br/>
            </asp:Panel>
        </div>
    </form>
</body>
</html>
